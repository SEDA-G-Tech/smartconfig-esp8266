#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>

const char* SOFTWARE_VERSION = "this is your software version"; //eg. SmartConfig-v1.0.0
const char* DEVICE_ID = "this is your device id";  //eg. WhizPad-WP0000
const char* COMPANY = "SEDA G-Tech";

void setup() {
  Serial.begin(115200);
  /* Do not change to other mode */
  WiFi.mode(WIFI_STA);
  smartConfig();
}

void loop() {
  delay(1000);
}
