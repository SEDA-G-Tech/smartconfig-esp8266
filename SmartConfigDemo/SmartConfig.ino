int conWiFi = 0, wifi = 0;
boolean conWiFiIndex = false;

void smartConfig() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);

  delay(500);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    wifi++;
    Serial.println(wifi);
    if (wifi >= 40) {
      break;
    }
  }

  if (wifi >= 40) {
    WiFi.beginSmartConfig(15);
    while (WiFi.status() != WL_CONNECTED) {
      digitalWrite(LED_BUILTIN, LOW);
      delay(250);
      digitalWrite(LED_BUILTIN, HIGH);
      delay(250);

      Serial.print(".");
      Serial.println(WiFi.smartConfigDone());

      if (WiFi.getSmartConfigStatus() == 1) {
        conWiFi++;

        if (conWiFi > 60) {
          Serial.println("STOP");
          WiFi.stopSmartConfig(); delay(50);
          WiFi.beginSmartConfig(15); conWiFi = 0;
        }
      }
      else if (WiFi.getSmartConfigStatus() >= 2) {
        conWiFi++;
        if (conWiFiIndex == false) {
          conWiFi = 0; conWiFiIndex = true;
        }

        if (conWiFi > 60) {
          WiFi.stopSmartConfig(); delay(50);
          WiFi.beginSmartConfig(15); conWiFi = 0;
          conWiFiIndex = false;
        }
      }
    }
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  digitalWrite(LED_BUILTIN, LOW);

  if (!MDNS.begin(DEVICE_ID, WiFi.localIP(), 20)) {
    Serial.println("Error setting up MDNS responder!");
    while (1) {
      delay(1000);
    }
  }
  uint8_t mac[WL_MAC_ADDR_LENGTH];
  WiFi.softAPmacAddress(mac);
  char infoJson[100];
  sprintf(infoJson, "{\"mac\":\"%02X:%02X:%02X:%02X:%02X:%02X\",\"com\":\"%s\",\"version\":\"%s\"}",
          mac[WL_MAC_ADDR_LENGTH - 6], mac[WL_MAC_ADDR_LENGTH - 5], mac[WL_MAC_ADDR_LENGTH - 4],
          mac[WL_MAC_ADDR_LENGTH - 3], mac[WL_MAC_ADDR_LENGTH - 2], mac[WL_MAC_ADDR_LENGTH - 1],
          COMPANY, SOFTWARE_VERSION);
  const char* infoJsonStr = infoJson;
  Serial.println(infoJsonStr);
  MDNS.addService("G-Tech", "tcp", 80);
  MDNS.addServiceTxt("G-Tech", "tcp", "info", infoJsonStr);
}

